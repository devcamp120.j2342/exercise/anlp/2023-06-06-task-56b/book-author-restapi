package com.devcamp.bookauthorrestapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthorrestapi.models.Author;
import com.devcamp.bookauthorrestapi.models.Book;

@RestController
@RequestMapping("/")
@CrossOrigin
public class BookAuthorRestAPIController {
    @GetMapping("/books")
    public ArrayList<Book> getBook(){
        Author author1 = new Author("khoi", "khoi@gmail.com", 'N');
        Author author2 = new Author("An", "an@gmail.com", 'N');
        Author author3 = new Author("John", "john@gmail.com", 'N');

        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);

        Book book1 = new Book("Tây du kí", author1, 68900, 1);
        Book book2 = new Book("đen", author2, 70000, 1);
        Book book3 = new Book("Đánh cá", author3, 69000, 1);

        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);

        ArrayList<Book> arrListBook = new ArrayList<Book>();

        arrListBook.add(book1);
        arrListBook.add(book2);
        arrListBook.add(book3);

        return arrListBook;
    }
}
