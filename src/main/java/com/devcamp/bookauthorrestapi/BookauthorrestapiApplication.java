package com.devcamp.bookauthorrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookauthorrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookauthorrestapiApplication.class, args);
	}

}
